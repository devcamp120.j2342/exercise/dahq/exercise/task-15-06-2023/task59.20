package com.devcamp.ordercontrollerapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.ordercontrollerapi.model.COrder;

public interface COrderRepository extends JpaRepository<COrder, Long> {

}
