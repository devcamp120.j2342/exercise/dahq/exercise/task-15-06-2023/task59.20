package com.devcamp.ordercontrollerapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrderControllerApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrderControllerApiApplication.class, args);
	}

}
