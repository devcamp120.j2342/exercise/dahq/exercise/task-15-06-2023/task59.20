package com.devcamp.ordercontrollerapi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.ordercontrollerapi.model.COrder;
import com.devcamp.ordercontrollerapi.repository.COrderRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class COrderController {
    @Autowired
    COrderRepository orderRepository;

    @GetMapping("/orders")
    public ResponseEntity<List<COrder>> getAllOrder() {
        try {
            List<COrder> orders = new ArrayList<COrder>();
            orderRepository.findAll().forEach(orders::add);
            return new ResponseEntity<>(orders, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

}
